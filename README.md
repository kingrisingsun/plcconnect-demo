# OPCUAMiloDemo

#### 介绍
JAVA基于S7Connect 连接PLC。
JAVA基于Milo，使用OPCUA方式连接PLC。

#### 使用说明
MILO
1.  修改OPCUAServiceImpl 中的PLC地址：endpointUrl；
2.  修改OpcNodeList中订阅地址后启动项目即可。

S7Connect
1. 修改S7ConnectUti 中的PLC地址，调用方法即可